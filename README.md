# Android Testing example on Gitlab CI

This project is a repo showing an example of using the Gitlab CI with Android.

This contains the starting repo after GitLab CI being set up in the blog post.
If you'd like to see what the repo looks like before, checkout the [`without-ci`]()
branch.

INSERT BLOG POST LINK HERE
